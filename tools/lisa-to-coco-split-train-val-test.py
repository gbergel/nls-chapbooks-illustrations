# Export LISA project annotations into COCO annotation format and split them into
# training, validation and test set
#
# Author: Abhishek Dutta
# Date: 02-Sep-2021
#
# Run using: $python3 lisa-to-coco-split-train-val-test.py --lisa_project_fn=lisa_project_5a1b_20200618_15h33m.json
#
# Updates:
# - append update log here in the format: Date : Author : Comment


import json
from json import encoder
import random
import os
import datetime
import argparse
import sys

FLOAT_PRECISION = 2
POSITIVE_COUNT = 3629
NEGATIVE_COUNT = 43700
SPLIT_NAME_LIST = ['train', 'val', 'test']
POSITIVE_SPLIT_LIST = [0, 3000, 3229, 3629]
NEGATIVE_SPLIT_LIST = [0, 3000, 3229, 43700]

def lisa_export(lisa_project_fn):
    with open(lisa_project_fn) as f:
        project = json.load(f)

    # collect a list of all file_indices that contain a region
    positive_findex_list = []
    negative_findex_list = []
    for findex, file in enumerate(project['files']):
        if len(project['files'][findex]['regions']):
            positive_findex_list.append(findex)
        else:
            negative_findex_list.append(findex)
    print('positive=%d, negative=%d' % (len(positive_findex_list), len(negative_findex_list)))
    print('Before')
    print(positive_findex_list[0:10])
    print(negative_findex_list[0:10])
    random.shuffle(positive_findex_list)
    random.shuffle(negative_findex_list)
    print('After')
    print(positive_findex_list[0:10])
    print(negative_findex_list[0:10])
    split_findex_list = {}
    for split_index, split_name in enumerate(SPLIT_NAME_LIST):
        positive0 = POSITIVE_SPLIT_LIST[split_index]
        positive1 = POSITIVE_SPLIT_LIST[split_index + 1]
        negative0 = NEGATIVE_SPLIT_LIST[split_index]
        negative1 = NEGATIVE_SPLIT_LIST[split_index + 1]
        split_findex_list[split_name] = positive_findex_list[positive0:positive1] + negative_findex_list[negative0:negative1]
        print('%s : %d:%d, %d:%d, %d' % (split_name, positive0, positive1, negative0, negative1, len(split_findex_list[split_name])))

        unique_annotaion_id = 1
        coco = init_coco_template()
        coco['categories'].append( {'id':1, 'name':'early_printed_illustration', 'supercategory':'illustration'} )
        for i in range(0, len(split_findex_list[split_name])):
            findex = split_findex_list[split_name][i]
            coco['images'].append({
                'id':findex,
                'width':project['files'][findex]['fdata']['width'],
                'height':project['files'][findex]['fdata']['height'],
                'file_name':project['files'][findex]['src'],
                'license':0,
                'flickr_url':'',
                'coco_url':'',
                'date_captured':''
            })
            for rindex in range(0, len(project['files'][findex]['regions'])):
                coco['annotations'].append({
                    'id':unique_annotaion_id,
                    'image_id':findex,
                    'category_id':1,
                    'segmentation':[ bbox_to_segmentation(project['files'][findex]['regions'][rindex]) ],
                    'area':bbox_to_area(project['files'][findex]['regions'][rindex]),
                    'bbox':project['files'][findex]['regions'][rindex],
                    'iscrowd':0
                });
                unique_annotaion_id = unique_annotaion_id + 1
        # write coco to json file
        outdir = os.path.dirname(lisa_project_fn)
        prefix = os.path.splitext(lisa_project_fn)[0]
        coco_fn = os.path.join(outdir, prefix + '-coco-' + split_name + '.json')
        with open(coco_fn, 'w') as f:
            json.dump(coco, f)
            print('  Written COCO dataset to %s' % (coco_fn))

        
    
def init_coco_template():
    coco = { 'info':{}, 'images':[], 'annotations':[], 'licenses':[], 'categories':[] };
    coco['info'] = { 'year': 2021,
                     'version': '1.0',
                     'description': 'Manual Annotations',
                     'contributor': 'Abhishek Dutta',
                     'url': 'http://www.robots.ox.ac.uk/~vgg/software/lisa/',
                     'date_created': str(datetime.datetime.now()),
    };
    coco['licenses'] = [ {'id':0, 'name':'Unknown License', 'url':''} ] # indicates that license is unknown
    return coco

def bbox_to_segmentation(bbox):
    if len(bbox) == 4:
        bbox[0] = round(bbox[0], FLOAT_PRECISION)
        bbox[1] = round(bbox[1], FLOAT_PRECISION)
        bbox[2] = round(bbox[2], FLOAT_PRECISION)
        bbox[3] = round(bbox[3], FLOAT_PRECISION)
        return [ bbox[0], bbox[1], bbox[0]+bbox[2], bbox[1], bbox[0]+bbox[2], bbox[1]+bbox[3], bbox[0], bbox[1]+bbox[3] ]
    else:
        return bbox

def bbox_to_area(bbox):
    if len(bbox) == 4:
        return round(bbox[2] * bbox[3], FLOAT_PRECISION)
    else:
        return -1

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Convert manual annotations from a LISA project to COCO format")
    parser.add_argument("--lisa_project_fn",
                        required=True,
                        type=str,
                        help="location of LISA project containing annotations")
    args = parser.parse_args()
    lisa_export(args.lisa_project_fn)
