  * **[ 22-Jun-2022]**: Added [a script](https://gitlab.com/vgg/nls-chapbooks-illustrations/-/blob/master/Illustration-Detector.md#using-the-trained-illustration-detector) to apply the illustration detector to a batch of images and export detection results as JSON file and images.
  * **[ 06-Sep-2021]**: See [Illustration-Detector](Illustration-Detector.md), if you just want to reuse the trained illustration detector. Visit our <a href="https://www.robots.ox.ac.uk/~vgg/research/chapbooks/">project page</a> for more details.

# Visual Analysis of Chapbooks Printed in Scotland
The [NLS Chapbooks](https://data.nls.uk/data/digitised-collections/chapbooks-printed-in-scotland/) contains 3000 chapbooks printed in Scotland. Each chapbook usually contains 8, 12, 16 or 24 pages. The chapbooks were produced cheaply to create everyday reading material and were the most popular reading material for the masses [1]. This dataset has been made freely available by the National Library of Scotland (NLS) and is available for download from the following website: [https://data.nls.uk/data/digitised-collections/chapbooks-printed-in-scotland/](https://data.nls.uk/data/digitised-collections/chapbooks-printed-in-scotland/).

Some of these chapbooks include an illustration. The illustrations contained in chapbooks were produced using crude woodcuts[1]. Furthermore, to reduce printing costs, these woodcuts were reused across multiple chapbooks. When the chapbooks circulated during the end of the 17th and until the late 19th century, the illustrations contained in these chapbooks provided contextual visual stimuli to the readers and often appeared in the title pages. In modern times, these illustrations are a valuable resources and evidence for researchers who are working to:
 - attribute printer or place of printing for chapbooks issues with no or false information about the publisher,
 - compare and qualitatively analyse all the illustrations used by a particular printer, and
 - study how the woodcuts were reused and shared amongst printers.

The goal of [this project](http://gitlab.com/vgg/nls-chapbooks-illustrations) is to help researchers pursue these and many other related research questions using software tools based on computer vision. As a part of this project, we are releasing free and open source software tools and metadata that allows researchers to answer these and many other research questions related to the NLS Chapbooks dataset. The workflow implemented in this project is visually depicted below.

<img src="data/images/overview.png" width="100%" title="An overview of our contribution described in this paper that enables visual analysis of chapbooks printed in Scotland. An object detector is trained to detect illustration regions in pages of $3000$ chapbooks. These regions enable the VGG Image Search Engine (VISE) software to visually search and group illustrations based on their visual content. Such visual search capability and grouping of illustrations allows researchers to forensically analyse book pages and metadata within the chapbooks dataset.">

This repository contains software tools that deliver the following functionality:

1. [Automatic Detection of Illustrations](#1-automatic-detection-of-illustrations): The [NLS Chapbooks dataset](https://data.nls.uk/data/digitised-collections/chapbooks-printed-in-scotland/) contains images that correspond to individual pages of a chapbook. Most of the chapbook pages do not contain any illustration while some contain one or more illustrations. If we want to visually search and analyse these illustrations, we need to find out which of the pages contains an illustration and the location of those illustrations within the page. As a part of this project, we have developed a workflow that enables automatic detection of illustrations in the chapbooks dataset.

2. [Visual Search of the Illustrations](#2-visual-search-of-the-illustrations): Based on automatically detected location of illustrations (see 1 above) and the VGG Image Search Engine ([VISE](https://gitlab.com/vgg/vise)) software, we describe the process of creating a [visual search engine](http://meru.robots.ox.ac.uk/nls_chapbooks/) based on the illustrations contained in the chapbooks dataset. This allows visual search of the illustrations using image region as a search query. Such a visual search capability allows researchers to find all the instances of reuse of a particular illustration -- or parts of an illustration -- in the chapbooks dataset. The chapbooks dataset already contains a rich set of metadata associated with each chapbook. This metadata is included as a part of the full [NLS Chapbooks dataset](https://data.nls.uk/data/digitised-collections/chapbooks-printed-in-scotland/) and is contained in [METS](https://en.wikipedia.org/wiki/Metadata_Encoding_and_Transmission_Standard) formatted XML files. We import this metadata in the visual search engine to allow search based on image as well as text metadata.

3. [Grouping of Illustrations based on Visual Similarity](#3-grouping-of-illustrations-based-on-visual-similarity): Using the visual search engine (see 2 above), we grouped illustrations based on the similarity of their visual content. In other words, each group contains all the illustrations that have similar content. Such groups are a valuable resource for researchers involved in tracing the use of reuse of woodblocks.

Below, we describe each of these functionality in more detail.

While the software tools developed in this project are aimed at the NLS Chapbooks dataset, we believe that these tools can easily be extended to pursue similar research goals for other datasets. We have therefore released all the software tools developed as a part of this project as open source tools to  encourage such extensions of our project.

## 1. Automatic Detection of Illustrations
The recent advancements in deep learning has enabled development of a family of deep neural networks that can automatically recognize common objects like bicycle, umbrella, bus, teddy bear, apple, etc. in large collections of images more accurately and quickly. These deep neural networks are called object detectors. We repurposed one of these object detectors to automatically detect illustrations in early printed books like the NLS Chapbooks. Since these object detectors are already very good at detecting common objects, they can quickly learn (i.e. they can be easily retrained) to detect novel object categories -- like an early printed illustration -- using a small number of exemplar images. For this work, we used the EfficientDet [2] object detector. EfficientDet is a family of neural net architecture for object detectors that attains very high accuracy while being light in weight (both model size and computational cost). Pre-trained models of EfficientDet are available under a permissive license that allows unrestricted use in academic projects and commercial products. The pretrained models can detect 80 object categories (e.g. person, bicycle, umbrella, bus, teddybear, apple, etc. ) defined by the COCO Object Detection Task. While these 80 object categories are commonplace and diverse, they are not sufficient for our requirement of detecting printed illustrations. Therefore, we retrain the EfficientDet object detector to automatically detect printed illustrations not only in chapbooks but also in other early printed books like the early printed books from the 15th and 16th centuries.

The chapbooks dataset [1] contains 47229 images most of which solely contains text while very few contain illustrations. We conduct the training of the EfficientDet object detector in two stages as shown in the figure below. 
In the first stage (i.e. Stage 1), we manually annotated a small number of illustrations
and used it to train the most light weight variant (i.e. the `efficientdet-d0`
model) of the EfficientDet detector. We applied this trained detector to automatically detect illustrations in a larger set of images that were not a part of the training set. These automatic detections were manually verified and corrected
(if required) by our domain expert. This manual verification step allowed us to
create a larger training dataset for the second stage (i.e. Stage 2) in which
we trained a more accurate variant (i.e. the `efficientdet-d2` model) of
the EfficientDet detector that requires a larger set of training examples.
The illustration detector resulting from Stage 2 was used to detect illustration
in the remaining images and these detections were manually verified to
create the final chapbooks illustration dataset.

<img src="data/images/dataset-annotation-workflow.png" title="Visual depiction of our semi-automatic workflow used to detect illustrations in the Chapbook pages." height="1000">

Now we provide a technical description of all the processes and software tools that we have allowed us to achieve the process described above. We use the List Annotator ([LISA](https://gitlab.com/vgg/lisa)) tool for manual annotation of rectangular regions occupied by illustrations in the images included in the chapbooks dataset. First, download the NLS chapbooks dataset from [https://data.nls.uk/data/digitised-collections/chapbooks-printed-in-scotland/](https://data.nls.uk/data/digitised-collections/chapbooks-printed-in-scotland/) and extract the downloaded <code>nls-data-chapbooks.zip</code> file to a folder (e.g. <code>/dataset/nls-data-chapbooks</code>. 
```
export MYSTORE=$HOME/nls    ## everything will be stored in this folder
mkdir $MYSTORE && cd $MYSTORE

## Download the NLS dataset of Chapbooks printed in Scotland
cd $MYSTORE/
wget -O nls-data-chapbooks.zip https://nlsfoundry.s3.amazonaws.com/data/nls-data-chapbooks.zip # 6.3GB
unzip nls-data-chapbooks.zip # extracts all the contents in "nls-data-chapbooks" folder

## Download our nls-chapbooks-illustrations repository
cd $MYSTORE/
git clone --recurse-submodules https://gitlab.com/vgg/nls-chapbooks-illustrations.git
```

Next, start the LISA application by opening the [tools/lisa.html](tools/lisa.html) file in a web browser. Now, click the "Browse" button under the "Create New Project" section and select the [data/annotations/nls-chapbooks-image-filename-list.txt](data/annotations/nls-chapbooks-image-filename-list.txt) file that contains a list of filenames of all images in the chapbooks dataset. This creates a new LISA project with all the images in the chapbooks dataset. Initially, the chapbook images are not visible because the LISA application does not know where to find the images. Press the "f" key and enter the location (for example: <code>/home/adutta/nls/dataset/nls_chapbooks/nls-data-chapbooks/</code>) of the chapbooks dataset. This will now allow LISA to load all the images and therefore you will now see all the images in the chapbooks dataset laid out in a grid as shown in the figure below. Now, we are ready to manually draw a bounding box (i.e. a rectangle) around an illustration contained in the first 4000 images of this dataset. For example, the image 1040 shows the title page of a chapbook and contains an illustration as shown in the figure below. To define a bounding box (shown as a yellow rectangle in figure below) for this illustration, first press the mouse button at the top left corner of this illustration, then drag the mouse cursor until the bottom right corner of this illustration is reached and release the mouse button to define the bounding box. When all the bounding boxes are created, the manual annotations can be saved by pressing the "Ctrl" + "s" key. To perform manual annotation over multiple sessions, you can save the project and load the saved project later on to resume the annotation process. 

<img src="data/screenshots/lisa-annotation-image-0-to-3999.jpg" height="400" title="Manual annotation of illustrations in the first 4000 images of the NLS Chapbooks dataset using the List Annotator (LISA) tool.">

There are 337 illustrations in the first 4000 images of the chapbooks dataset. We have already created these manual annotations and they are saved in the [data/annotations/step1-manual-annotation-image-0-to-3999.json](data/annotations/step1-manual-annotation-image-0-to-3999.json) file as a LISA project. We will use these manual annotations to retrain the [EfficientDet](https://github.com/google/automl/tree/master/efficientdet) object detector to automatically detect illustrations in the remaining images of the chapbooks dataset. Here is a list of Unix shell commands required to set up EfficientDet.
```
## Requirements:
## - python-3.8            (see https://www.python.org/downloads/)
## - NVidia CUDA and CUDNN (see https://developer.nvidia.com/cuda-downloads)

## install all the python dependencies (e.g. tensorflow, ...)
cd $MYSTORE
python3.8 -m venv $MYSTORE/venv
source $MYSTORE/venv/bin/activate  # ensure that command prompt is "(venv) ..."
$MYSTORE/venv/bin/python3.8 -m pip install --upgrade pip
pip install tensorflow==2.5.0 Pillow pytype cython tensorflow_model_optimization

## check if cuda and tensorflow installations are correct
## the following two commands should not show any errors like
## "Could not load dynamic library ..." or any other errors
python -c "import tensorflow as tf; print(tf.reduce_sum(tf.random.normal([1000, 1000])))"
python -c "import tensorflow as tf; print(tf.config.experimental.list_physical_devices('GPU'))"

## install COCO python API
pip install 'git+https://github.com/cocodataset/cocoapi.git#subdirectory=PythonAPI'

## Test the efficientdet code repository
cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet
python model_inspect_test.py   # (optional) to test setup, takes some time ...

## Extract efficientdet model files
mkdir $MYSTORE/nls-chapbooks-illustrations/data/efficientdet/
cd $MYSTORE/nls-chapbooks-illustrations/data/efficientdet/
wget https://storage.googleapis.com/cloud-tpu-checkpoints/efficientdet/coco/efficientdet-d0.tar.gz
wget https://storage.googleapis.com/cloud-tpu-checkpoints/efficientdet/coco/efficientdet-d2.tar.gz
tar -zxvf efficientdet-d0.tar.gz && tar -zxvf efficientdet-d2.tar.gz

## to check, apply efficientdet object detector on sample images
## detection results are saved to automl/efficientdet/testdata/0.jpg
cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet
python model_inspect.py --runmode=infer --model_name=efficientdet-d0 \
--ckpt_path=$MYSTORE/nls-chapbooks-illustrations/data/efficientdet/efficientdet-d0/ \
--input_image=$MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/testdata/img1.jpg  \
--output_image_dir=$MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/testdata/
```

Now that we have a working installation of EfficientDet, we can proceed with Stage 1 of our workflow. For Stage 1, we manually processed the first 4000 images
(i.e.\ from image index 0 to 3999) and manually marked rectangular regions
containing illustration. These manual annotations are stored in [data/annotations/step1-manual-annotation-image-0-to-3999.json](data/annotations/step1-manual-annotation-image-0-to-3999.json) file as a LISA project. We can retrain the EfficientDet object detector to detect illustrations in the chapbooks dataset using these manual annotations. Since the EfficientDet retraining process requires training data in tfrecord file format, we first convert the manual annotations saved as LISA project file to [COCO](https://cocodataset.org/#format-data) format using the [tools/lisa_to_coco.py](tools/lisa_to_coco.py) python script. Next, we convert the annotations in COCO format to tfrecord format using [automl/efficientdet/dataset/create_coco_tfrecord.py](automl/efficientdet/dataset/create_coco_tfrecord.py) python script provided with the EfficientDet code repository.
```
source $MYSTORE/venv/bin/activate
cd $MYSTORE/nls-chapbooks-illustrations/tools
python lisa_to_coco.py --lisa_project_fn=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step1-manual-annotation-image-0-to-3999.json

mkdir -p $MYSTORE/tfrecord/nls_chapbooks/stage1/
cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/
PYTHONPATH=".:$PYTHONPATH" python dataset/create_coco_tfrecord.py --logtostderr \
  --image_dir=$MYSTORE/nls-data-chapbooks/ \
  --object_annotations_file=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step1-manual-annotation-image-0-to-3999_train_coco.json \
  --output_file_prefix=$MYSTORE/tfrecord/nls_chapbooks/stage1/train \
  --num_shards=1
```

Now we are ready to start the training of the EfficientDet model using manual annotations created for Stage 1.
```
mkdir -p $MYSTORE/models/stage1/
cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/
python main.py --mode=train \
    --train_file_pattern=$MYSTORE/tfrecord/nls_chapbooks/stage1/train-*-of-00001.tfrecord \
    --model_name=efficientdet-d0 \
    --model_dir=$MYSTORE/models/stage1/  \
    --ckpt=$MYSTORE/nls-chapbooks-illustrations/data/efficientdet/efficientdet-d0 \
    --train_batch_size=8 \
    --num_examples_per_epoch=337 --num_epochs=10  \
    --hparams="num_classes=1,moving_average_decay=0" \
    --eval_after_train=False --tf_random_seed=9973
```
The `efficientdet-d0` model trained above is applied to automatically detect illustrations in the next 16000 images (i.e.\ from image with index 4000 to 19999) as follows.
```
ln -s $MYSTORE/nls-chapbooks-illustrations/tools/lisa_detect.py $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/
cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/
python model_inspect.py --runmode=saved_model \
  --model_name=efficientdet-d0 \
  --ckpt_path=$MYSTORE/models/stage1/ \
  --saved_model_dir=$MYSTORE/models/stage1/savedmodel \
  --hparams="num_classes=1,moving_average_decay=0"

PYTHONPATH=".:$PYTHONPATH" python lisa_detect.py --runmode=saved_model_lisa_infer \
  --model_name=efficientdet-d0 \
  --ckpt_path=$MYSTORE/models/stage1/ \
  --hparams="num_classes=1,moving_average_decay=0"  \
  --saved_model_dir=$MYSTORE/models/stage1/savedmodel \
  --in_lisa_project=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step1-manual-annotation-image-0-to-3999.json \
  --file_src_prefix=$MYSTORE/nls-data-chapbooks/ \
  --out_lisa_project=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step2-detections-image-4000-to-20000_GENERATED.json \
  --start_findex=4000 --end_findex=20000 \
  --min_score_thresh=0.6
```
We have provided the results of above code in [data/annotations/step2-detections-image-4000-to-20000.json](data/annotations/step2-detections-image-4000-to-20000.json) and we are now ready to move to Stage 2 of our workflow. 


In Stage 2 of our workflow, the automatic detections generated in Stage 1 were manually verified and corrected (if required) by our domain expert (Giles Bergel). We were able to compile a list of 1601 images containing 1728 illustrations in the first 20000 images (i.e. from
image index 0 to image index 19999) in the NLS Chapbooks dataset. These manually verified annotations are present in [data/annotations/step3-manual-verification-image-4000-to-20000.json](data/annotations/step3-manual-verification-image-4000-to-20000.json) and are used to train the `efficientdet-d2` model as follows.

```
export MYSTORE=$HOME/nls                 # we have been storing everything here
source $MYSTORE/venv/bin/activate
cd $MYSTORE/nls-chapbooks-illustrations/tools/
python lisa_to_coco.py --lisa_project_fn=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step3-manual-verification-image-4000-to-20000.json

mkdir -p $MYSTORE/tfrecord/nls_chapbooks/stage2/
cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/
PYTHONPATH=".:$PYTHONPATH" python dataset/create_coco_tfrecord.py --logtostderr \
  --image_dir=$MYSTORE/nls-data-chapbooks/ \
  --object_annotations_file=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step3-manual-verification-image-4000-to-20000_train_coco.json \
  --output_file_prefix=$MYSTORE/tfrecord/nls_chapbooks/stage2/train \
  --num_shards=8

mkdir -p $MYSTORE/models/stage2/
python main.py --mode=train \
    --train_file_pattern=$MYSTORE/tfrecord/nls_chapbooks/stage2/train-*-of-00008.tfrecord \
    --model_name=efficientdet-d2 \
    --model_dir=$MYSTORE/models/stage2/  \
    --ckpt=$MYSTORE/nls-chapbooks-illustrations/data/efficientdet/efficientdet-d2 \
    --train_batch_size=4 \
    --num_examples_per_epoch=1601 --num_epochs=50  \
    --hparams="num_classes=1,moving_average_decay=0" \
    --eval_after_train=False --tf_random_seed=9973

tensorboard --bind_all --logdir $MYSTORE/models/stage2/ # training progress (optional)
```

> We do not split our dataset into training, validation and testing set because the final stage of our workflow (i.e. Stage 3) involves manual verification of the detections by our domain expert. If this were not the case, we would split the full dataset into the following 3 parts: training set (70%), validation set (15%) and test set (15%). The validation set would be used for tuning the model hyper-parameters (e.g. batch size, number of epochs, etc.) and test would be used to evaluate performance on the selected model. We use this strategy to train the final model for illustration detection on the full dataset resulting from Stage 3 of our workflow (see below).

In Stage 3 of our workflow, we apply the trained model resulting from the Stage2 to detect illustrations in all the remaining 27329 images (i.e. from image with index 20000 to 47329) of the NLS Chapbooks dataset. These detections are available in [data/annotations/step4-detections-image-20000-to-47329.json](data/annotations/step4-detections-image-20000-to-47329.json). These automatic detections were again manually verified and corrected (if required) by our domain expert (GB). This workflow resulted in 3864 manually reviewed illustrations extracted from 3629 images containing chapbook pages. These annotations are available in [data/annotations/step5-manual-verification-image-20000-to-47329.json](data/annotations/step5-manual-verification-image-20000-to-47329.json). This completes the Stage 3 of our workflow.

We can evaluate performance of the illustration detector trained on
1728 illustrations appearing in chapbook images with index from
0 to 19999 (i.e. Stage 2) using a test set that contains 2136 manually reviewed annotations in chapbook images with index from 20000
to 47329 (i.e. Stage 3). Therefore, the test set did not contain any illustrations that were seen by the detector during the training phase. 

We first generate automatic detections using the model trained in Stage 3.
```
cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/
python model_inspect.py --runmode=saved_model \
  --model_name=efficientdet-d2 \
  --ckpt_path=$MYSTORE/models/stage2/ \
  --saved_model_dir=$MYSTORE/models/stage2/savedmodel \
  --hparams="num_classes=1,moving_average_decay=0"

PYTHONPATH=".:$PYTHONPATH" python lisa_detect.py --runmode=saved_model_lisa_infer \
  --model_name=efficientdet-d2 \
  --ckpt_path=$MYSTORE/models/stage2/ \
  --hparams="num_classes=1,moving_average_decay=0"  \
  --saved_model_dir=$MYSTORE/models/stage2/savedmodel \
  --in_lisa_project=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step3-manual-verification-image-4000-to-20000.json \
  --file_src_prefix=$MYSTORE/nls-data-chapbooks/ \
  --out_lisa_project=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step4-detections-image-20000-to-47329_GENERATED.json \
  --start_findex=20000 --end_findex=47329 \
  --min_score_thresh=0.6
```

Next, we compare these detections with manually verified annotations from Stage 3.
```
cd $MYSTORE/nls-chapbooks-illustrations/tools/
python compute-precision-recall.py \
  $MYSTORE/nls-chapbooks-illustrations/data/annotations/step4-detections-image-20000-to-47329_GENERATED.json \
  $MYSTORE/nls-chapbooks-illustrations/data/annotations/step5-manual-verification-image-20000-to-47329.json 20000 47329
  
	IoU threshold=0.50, precision=0.993, recall=0.911
	IoU threshold=0.75, precision=0.987, recall=0.905
	IoU threshold=0.95, precision=0.973, recall=0.892
```

The results above will vary for different users because of differences in the training procedure. The results presented in our paper can be reproduced by using the detections saved in [data/annotations/step4-detections-image-20000-to-47329.json](data/annotations/step4-detections-image-20000-to-47329.json):
```
cd $MYSTORE/nls-chapbooks-illustrations/tools/
python compute-precision-recall.py \
  $MYSTORE/nls-chapbooks-illustrations/data/annotations/step4-detections-image-20000-to-47329.json \
  $MYSTORE/nls-chapbooks-illustrations/data/annotations/step5-manual-verification-image-20000-to-47329.json 20000 47329
  
	IoU threshold=0.50, precision=0.993, recall=0.911
	IoU threshold=0.75, precision=0.987, recall=0.905
	IoU threshold=0.95, precision=0.973, recall=0.892
```

Finally, at the end of Stage 3 of our workflow, we have a dataset of 3864 illustrations extracted from 3629 images. We train a `efficientdet-d0` model using this full dataset and publicly release our final illustration detector. The [following section](Illustration-Detector.md#using-the-trained-illustration-detector) of [Illustration-Detector.md](Illustration-Detector.md) describes the process of using this pre-trained illustration detector on your own images. This pre-trained model can be useful in detecting illustrations in other datasets of similar nature. The process of obtaining this trained model is described in [Illustration-Detector.md](Illustration-Detector.md).


## 2. Visual Search of the Illustrations
Researchers can gain valuable insight into the NLS Chapbooks
dataset[1] if they can visually search through the 3864 illustrations contained in this dataset using an image region as a search query (i.e. <i>visual search</i>}) as shown below.

<img src="data/images/vise_query_search_example.jpg" width="90%" title="Example of visual search using an image region as a search query (yellow bounding box shown at the top) with results showing other images (bottom) that are visually similar to the query.">

We use the open source VGG Image Search Engine ([VISE](https://www.robots.ox.ac.uk/~vgg/software/vise/)) [3] software to create a [visually searchable database](http://meru.robots.ox.ac.uk/nls_chapbooks/) of illustrations contained in the chapbooks dataset (created in the previous section). In addition to the chapbook images, the NLS Chapbooks dataset also contains metadata (e.g. printer, author, date, etc.) associated with each of the chapbook images. Since the VISE software also supports text metadata search, we will also import these additional metadata into the VISE visual search engine.

```
## Tested in Ubuntu 18.04.5 and Debian 10.5

export MYSTORE=$HOME/nls                 # we have been storing everything here

## Download VISE software
cd $MYSTORE
mkdir $MYSTORE/vise-dep                  # store for VISE dependencies

git clone https://gitlab.com/vgg/vise.git
cd $MYSTORE/vise/scripts/build/
./make_deps_debian.sh $MYSTORE/vise-dep  # use ./make_deps_macos.sh for MacOS
                                         # ensure that final message is
                                         # "You can now proceed to compiling..."
cd $MYSTORE/vise
mkdir cmake_build && cd cmake_build
$MYSTORE/vise-dep/bin/cmake -DCMAKE_PREFIX_PATH=$MYSTORE/vise-dep/ ../src
make -j                                  # compile VISE

## copy images and metadata from NLS Chapbooks dataset
mkdir $MYSTORE/vise-project              # store for VISE project
source $MYSTORE/venv/bin/activate
cd $MYSTORE/nls-chapbooks-illustrations/tools

python lisa_to_vise.py \
  --lisa_project_fn=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step5-manual-verification-image-20000-to-47329.json \
  --vise_project_dir=$MYSTORE/vise-project/ \
  --nls_dataset_dir=$MYSTORE/nls-data-chapbooks/ \
  --nls_metadata_json_dir=$MYSTORE/nls-chapbooks-illustrations/data/metadata/nls/json/

# expected console output:
#   ...
#   Total image with region count = 3629
#   Total region count = 3864

## create visual search engine using VISE
cp $MYSTORE/nls-chapbooks-illustrations/data/vise/* $MYSTORE/vise-project/data/
ln -s $MYSTORE/vise-project/image_src/ $MYSTORE/vise-project/image
# inspect $MYSTORE/vise-project/data/conf.txt and make changes based on your
# requirements. For example, set nthread-indexing=16 if you have 16 CPU cores
# and set bow_descriptor_count=-1 to use all available descriptors for training. 

cd $MYSTORE/vise/cmake_build
./vise/vise-cli --cmd=create-project nls_chapbooks:$MYSTORE/vise-project/data/conf.txt
# the above command takes around 2-3 hours to complete.
# use the following command to show progress:
# tail -f $MYSTORE/vise-project/data/index.log
 
# next, serve the newly created search engine over http
./vise/vise-cli --cmd=serve-project \
  --http-www-dir=$MYSTORE/vise/src/www/ \
  --http-address=0.0.0.0 --http-port=9669 \
  --http-worker=4 --http-namespace=/ \
  nls_chapbooks:$MYSTORE/vise-project/data/conf.txt
```

The last command (i.e. `./vise/vise-cli`) loads the visual search engine for the NLS Chapbooks project and makes it accessible using a standard web browser at [http://localhost:9669/nls_chapbooks/](http://localhost:9669/nls_chapbooks/). Here are some browser screenshots showing the visual search engine interface for the NLS chapbooks project.

<img src="data/screenshots/vise-nls-chapbooks-project-home.jpg" height="400" title="Home page of the NLS Chapbooks project">

<img src="data/screenshots/vise-nls-chapbooks-project-file-view.jpg" height="400" title="A page showing a chapbook image and its metadata. The blue rectangular region shows the search query.">

<img src="data/screenshots/vise-nls-chapbooks-project-search-results.jpg" height="400" title="A search result page showing all the other chapbook images that visually match the search query image region.">

## 3. Grouping of Illustrations based on Visual Similarity
VISE software can also group together illustrations based on the similarity of their visual content. Such visual grouping of illustration allows researchers to study the reuse of illustrations among the chapbooks.

The VISE software runs visual search queries using the known location of each illustration in the chapbook images. Each of these search queries results in a set of matches shown below. This procedure generates a <i>match graph</i> whose nodes correspond to a unique illustration and whose edges depict a visual match between the search query node and
match node. We only retain match results that are above a certain score threshold to ensure that only high quality matches get represented in the match graph. The [depth-first search](https://en.wikipedia.org/wiki/Depth-first_search) graph exploration procedure applied to the match graph allows us to discover all the visual groups
(i.e. groups of illustrations that have similar visual content) in the illustration dataset as shown below.

<a href="http://meru.robots.ox.ac.uk/nls_chapbooks/visual_group?group_id=illustration-group-40&set_id=183"><img src="data/images/vise_illustration_visual_group_example.jpg" height="90%" title="Example of a visual group of illustrations that have similar visual content (highlighted using yellow bounding box) and were most likely printed using the same printing surface, probably a stereotype plate."></a>

Here is the VISE command to generate visual grouping of illustrations.

```
export MYSTORE=$HOME/nls                 # we have been storing everything here
cd $MYSTORE/vise/cmake_build
./vise/vise-cli --cmd=create-visual-group \
  --vgroup-id=illustration-group-40 \
  --vgroup-name="Illustration Group" \
  --vgroup-description="Visual groupings of all illustrations present in the NLS chapbooks dataset" \
  --query-type=region \
  --max-matches=50 --min-match-score=25 --vgroup-min-score=40 \
  --filename-like=% --match-iou-threshold=0.65 \
  nls_chapbooks:$MYSTORE/vise-project/data/conf.txt

# The above command takes around 2 minutes and generates a sqlite 
# database $MYSTORE/vise-project/data/ilustration-group-40.sqlite
#
# The final output of this command should show:
# vise::project:: vgroup-min-score=40, match graph vertices=1764, number of queries=1764
# vise::project:: visual group contains 481 connected components (found in 17 ms)

## update project configuration to let VISE load this visual group
echo "visual-group-id-list=illustration-group-40" >> $MYSTORE/vise-project/data/conf.txt

# next, serve the newly created search engine along with 
# visual groups over http
./vise/vise-cli --cmd=serve-project \
  --http-www-dir=$MYSTORE/vise/src/www/ \
  --http-address=0.0.0.0 --http-port=9669 \
  --http-worker=4 --http-namespace=/ \
  nls_chapbooks:$MYSTORE/vise-project/data/conf.txt
  
# the above command should show the following in console window
# vise::project : initialized following 1 visual groups: illustration-group-40,

# To visualize the groups, open the following URL in your web browser
# http://localhost:9669/nls_chapbooks/visual_group?group_id=illustration-group-40
```

The [following website](http://meru.robots.ox.ac.uk/nls_chapbooks/visual_group?group_id=illustration-group-40) shows all the illustrations in the NLS chapbooks dataset grouped based on their visual similarity.

## Frequently Asked Questions (FAQ)
All the code and instructions included in this code repository were installed and were verified to work as expected on 30-June-2021 in the following environment:
```
Operating System : Ubuntu 18.04.5 LTS
Python           : Python 3.8.8
gcc/g++          : gcc/g++ 7.5.0
VISE             : https://gitlab.com/vgg/vise (commit 8e494355b0)
EfficientDet     : https://github.com/google/automl (commit affe19b870)
CUDA             : CUDA Version: 11.3
Nvidia Driver    : Driver Version: 465.19.01
GPU              : NVIDIA Corporation GK110B [GeForce GTX TITAN Black] 12Gb	
```

## Citation
Please cite this work as follows:

<cite>Dutta Abhishek, Bergel Giles, and Zisserman Andrew. 2021. Visual Analysis of Chapbooks Printed in Scotland. In The 6th International Workshop on Historical Document Imaging and Processing (HIP '21), September 5–6, 2021, Lausanne, Switzerland. ACM, New York, NY, USA 6 Pages. <a href="https://doi.org/10.1145/3476887.3476893">https://doi.org/10.1145/3476887.3476893</a></cite>

Here is [a copy](doc/dutta2021visual.pdf) of this paper: [doc/dutta2021visual.pdf](doc/dutta2021visual.pdf)

## References
[1] National Library of Scotland. Chapbooks printed in Scotland. National Library of Scotland, 2019. [https://doi.org/10.34812/vb2s-9g58](https://doi.org/10.34812/vb2s-9g58)

[2] Tan, Mingxing, Ruoming Pang, and Quoc V. Le. "Efficientdet: Scalable and efficient object detection." Proceedings of the IEEE CVPR. 2020. https://arxiv.org/abs/1911.09070

[3] VGG Image Search Engine, https://www.robots.ox.ac.uk/~vgg/software/vise/


## Contact
Contact [Abhishek Dutta](adutta@robots.ox.ac.uk) for feedback and queries related to this code repository.
