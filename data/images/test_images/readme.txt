Image Downloaded from British Library

1. Tyndale’s New Testament, 1526 [ British Library ]
  - https://www.bl.uk/collection-items/william-tyndales-new-testament
  - https://www.bl.uk/britishlibrary/~/media/bl/global/dst%20discovering%20sacred%20texts/collection%20items/tyndales-new-testament-1526-c_188_a_17_f001r.jpg

2. From sacred scriptures to the people’s Bible [ British Library ] 
  - https://www.bl.uk/sacred-texts/articles/from-sacred-scriptures-to-the-peoples-bible

